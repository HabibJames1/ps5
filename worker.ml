open Async.Std

module Make (Job : MapReduce.Job) = struct
  
  module WR = Protocol.WorkerRequest(Job)
  module WP = Protocol.WorkerResponse(Job)

  (* see .mli *)
  let run r w =
    WR.receive r >>= fun msg -> 
      match msg with 
      | `Eof -> return (WP.send w (WP.JobFailed "Failure"))
      | `Ok(t) -> begin
        match t with 
          |WR.MapRequest (inp) -> begin
            Job.map inp >>| fun i -> 
              WP.send w (WP.MapResult i)
          end
          |WR.ReduceRequest (k,lst) -> begin
            Job.reduce (k,lst) >>| fun o ->
              WP.send w (WP.ReduceResult o)
          end 
      end
end

(* see .mli *)
let init port =
  Tcp.Server.create
    ~on_handler_error:`Raise
    (Tcp.on_port port)
    (fun _ r w ->
      Reader.read_line r >>= function
        | `Eof    -> return ()
        | `Ok job -> match MapReduce.get_job job with
          | None -> return ()
          | Some j ->
            let module Job = (val j) in
            let module Mapper = Make(Job) in
            Mapper.run r w
    )
    >>= fun _ ->
  print_endline "server started";
  print_endline "worker started.";
  print_endline "registered jobs:";
  List.iter print_endline (MapReduce.list_jobs ());
  never ()