open Async.Std

let l = ref [] (*List of where_to_connects*)
let working_workers = ref 0 (*a list of functioning workers*)
let q= ref (AQueue.create ()) (*a queue of available workers*)

let init addrs =
	l := List.map (fun (str, num)-> (Tcp.to_host_and_port str num)) addrs;
	working_workers := List.length !l

module Make (Job : MapReduce.Job) = struct

	(*called once in map_reduce; creates connection to every address
	and pushes connections onto queue q*)
	let connect_init lst= 
	  	List.iter (fun el->
	  		ignore (try_with (fun () -> (Tcp.connect el)) >>=
	  			fun ob ->
	  			match ob with
	  				|Core.Std.Error _ -> return ()
	  				|Core.Std.Ok (s,r,w) -> 
	  				 	Writer.write_line w Job.name;
	  				 	return (AQueue.push !q (s,r,w))
	  				 	)) lst


	module WR = Protocol.WorkerRequest(Job)
	module WP = Protocol.WorkerResponse(Job)
	module C = Combiner.Make(Job)


	let do_work_map queue input= 
		let rec retry () = 
			if !working_workers= 0 then failwith "Infrastructure Failure1" else
			AQueue.pop queue >>= fun element ->
			let (_,r,w) = element in
			WR.send w (WR.MapRequest(input));
			WP.receive r >>= fun response ->
			match response with 
				| `Eof-> ignore (Reader.close r);
					working_workers := !working_workers - 1;
					retry ()
				| `Ok (result) -> begin
					match result with
						|WP.JobFailed (str) -> failwith str
						|WP.MapResult (inter) -> 
						AQueue.push queue element;
						return inter
						| _ -> ignore (Reader.close r);
						working_workers := !working_workers - 1;
						retry ()
				end
			in
			retry ()

	let do_work_reduce queue input= 
	let rec retry () =
		if !working_workers= 0 then failwith "Infrastructure Failure" else
		AQueue.pop queue >>= fun element ->
		let (_,r,w) = element in
		match input with 
			| (key,lst) ->  WR.send w (WR.ReduceRequest(key,lst));
				WP.receive r >>= fun response ->
					match response with 
						| `Eof-> ignore (Reader.close r);
						working_workers := !working_workers - 1;
						retry ()
						| `Ok (result) -> begin
							match result with
								|WP.JobFailed (str) -> failwith str
								|WP.ReduceResult (out) -> 
								AQueue.push queue element;
								return (key, out)
								| _ -> ignore (Reader.close r);
								working_workers := !working_workers - 1;
								retry ()
						end
					in
					retry ()


  let map_reduce inputs =
    connect_init !l;
	Deferred.List.map inputs (fun el -> do_work_map !q el) 
    >>| List.flatten
    >>| C.combine
    >>= fun inters ->
    Deferred.List.map inters (fun el-> do_work_reduce !q el)
     

end