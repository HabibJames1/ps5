open Async.Std
open Async_unix

(******************************************************************************)
(** input and output types                                                    *)
(******************************************************************************)

type id = int
type dna_type = Read | Ref

type sequence = {
  id   : id;
  kind : dna_type;
  data : string;
}

(** Indicates a matching subsequence of the given read and reference *)
type result = {
  length   : int;

  read     : id;
  read_off : int;

  ref      : id;
  ref_off  : int;
}

(******************************************************************************)
(** file reading and writing                                                  *)
(******************************************************************************)

(** Convert a line into a sequence *)
let read_sequence line = match Str.split (Str.regexp "@") line with
  | [id; "READ"; seq] -> {id=int_of_string id; kind=Read; data=seq}
  | [id; "REF";  seq] -> {id=int_of_string id; kind=Ref;  data=seq}
  | _ -> failwith "malformed input"

(** Read in the input data *)
let read_files filenames : sequence list Deferred.t =
  if filenames = [] then failwith "No files supplied"
  else
    Deferred.List.map filenames Reader.file_lines
      >>| List.flatten
      >>| List.map read_sequence


(** Print out a single match *)
let print_result result =
  printf "read %i [%i-%i] matches reference %i [%i-%i]\n"
         result.read result.read_off (result.read_off + result.length)
         result.ref  result.ref_off  (result.ref_off  + result.length)

(** Write out the output data *)
let print_results results : unit =
  List.iter print_result results

(******************************************************************************)
(** Dna sequencing jobs                                                       *)
(******************************************************************************)

module Job1 = struct
  type input = sequence
  type key = string
  type inter = sequence * int
  type output = result list

  let name = "dna.job1"


  let map input : (key * inter) list Deferred.t =
  
      (* create_kmers should take a string and return a list of (kmer,  (sequence, offset))  *)

       let rec create_kmers input str acc_list acc_offset  =  if (String.length str) <  10 then acc_list else
               create_kmers  input (String.sub str 1 ((String.length str) - 1))
                   ( ((String.sub str 0 10), (input, acc_offset)) :: acc_list)  (acc_offset +1 )  in

      
        return (create_kmers input input.data [] 0)
           
  (* key is a kmer, inters is a list of tuples  of (sequence, offset). Output will be a list of results  *)

  let reduce (key, inters) : output Deferred.t =
       let rec helper key inters acc= match inters with 
             |[] -> acc
             |hd::tl ->  helper key tl 
               ((List.fold_left (fun acc2 a -> if (fst a).kind = (fst hd).kind 
                              then acc2 else (result 10 ((fst hd).id) (snd hd) ((fst a).id) (snd a)):: acc2) [] tl)@ acc) in

      return ( helper key inters [])




  
                           

end

let () = MapReduce.register_job (module Job1)
 


module Job2 = struct
  type input = (key * inter) list
  type key = id * id
  type inter = result 
  type output =  result list

  let name = "dna.job2"

   (* will take in a result and return a single (key * inter)  *)
  let map input : (key * inter) list Deferred.t =
           return ((( key.read, key.ref), key) :: [])

   (* will take a key of type result and a list of all results of kmers in these two ids *)
  let reduce (key, inters) : output Deferred.t =
         let offset_reads = List.fold_left (fun acc a -> a.read_off :: acc)   []  inters    (* a list of the offsets of reads *)
         let offset_refs = List.fold_left (fun acc a ->  a.ref_off:: acc )   [] inters     (* a list of the offsets of refs *)
         let ordered_reads = List.sort (fun a b -> a -b) offset_reads    (* a list of the offsets for read in order *)
         let orderd_refs = List.sort (fun a b -> a -b) offset_refs  (* a list of the offsets for ref in order *)
  
         (* THIS FUNCTION DOESNT WORK DIDNT FINNISH.  BUT given the ordered_reads and ordered_refs if the values next to them are one apart in one the corresponding values should be next to each other in the other.  So i was trying to fold across one of them with an accumulator that store in its first value a list of consecutive offsets, and in its second value a list of lists of consecutive values.  Whenever the first calue in the adjacent_reads is more than one apart from the first value in the first part of the tuple, it the first list in the tuple accumulator will be added to the list of lists and a new one  started in the second.  THere probably is an easier way to do this- just how I was trying to implement.  should return a list of lists of consecutive values *)
         Let adjacent_reads = List.fold_left 
                  (fun acc a -> match (fst acc) with          
                              |[] -> ( a::[], snd acc) 
                              |_-> if  a- (List.hd (fst acc)) = 1 then (a:: (fst acc), snd acc) else (a::[], (fst acc :: snd acc))      
                                                        ) ([], [])  ordered_reads

      (* should return a list of start values and length *)
        
                  
      
         
         
        
        
end

let () = MapReduce.register_job (module Job2)



module App  = struct

  let name = "dna"

  module Make (Controller : MapReduce.Controller) = struct
    module MR1 = Controller(Job1)
    module MR2 = Controller(Job2)

    let run (input : sequence list) : result list Deferred.t =
        failwith "lol"

    let main args =
      read_files args
        >>= run
        >>| print_results
  end
end

let () = MapReduce.register_app (module App)

